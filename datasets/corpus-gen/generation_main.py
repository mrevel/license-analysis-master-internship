import random
import sys


class Rule:
	"""
		ODRL Rule.
		Must have a modality.
		May have an assignee.
		Must have 0, 1 or many actions.
		Must have 0, 1 or many targets.
	"""
	def __init__(self, mod, assignee=None, actions=[], targets=[]):
		self.mod = mod #ex "odrl:permission"
		self.assignee = assignee
		self.actions = actions
		self.targets = targets
	def __str__(self):
		s = f"\t{self.mod} [\n"
		if self.targets != []: s += f"\t\todrl:target {' , '.join(self.targets)} ;\n"
		if self.assignee != None: s += f"\t\todrl:assignee {self.assignee} ;\n"
		s += f"\t\todrl:action {' , '.join(self.actions)}\n\t]"
		return s
		
class Policy:
	"""
		ODRL Policy.
		Must have 0, 1 or many rules.
	"""
	def __init__(self):
		self.rules = []
	def __str__(self):
		s = "\
@prefix cc: <http://creativecommons.org/ns#>.\n\
@prefix odrl: <http://www.w3.org/ns/odrl/2/>.\n\n\
:license a odrl:Policy ;\n"
		s += ' ;\n'.join([str(rule) for rule in self.rules]) + " .\n"
		return s




# Retrieve sentence and ODRL information from lexicon files
def parse_lexicon_file(fname):
	with open(fname, "r") as f:
		lines = f.readlines()
		return [line[:-1] for line in lines]
lexicons = {
	"actions"    : parse_lexicon_file("lexicons/actions.txt"),
	"actions_p"  : parse_lexicon_file("lexicons/actions_passive.txt"),
	"modalities" : parse_lexicon_file("lexicons/modalities.txt"),
	"actors"     : parse_lexicon_file("lexicons/actors.txt"),
	"targets"    : parse_lexicon_file("lexicons/targets.txt")
}



# Util

def sentencify(sentence):
	return sentence[0].upper() + sentence[1:] + "\n"

def pick_from_lex(lex_name):
	tok_uri = random.choice(lexicons[lex_name]).split("|")
	return (tok_uri[0], tok_uri[1])

def pick_new_from_lex(lex_name, old_tok):
	tok, uri = old_tok, None
	while tok == old_tok:
		tok, uri = pick_from_lex(lex_name)
	assert uri != None
	return tok, uri


# Main function

"""
	proba_passive:  probability for the sentence to have no assignee
	proba_conj_act: probability to have several actions
	proba_conj_tar: probability to have several targets (disjoint with actions)
	
	Output: (str, str) The generated sentence with the corresponding ODRL
	graph (serialized in Turtle)
"""
def gen_sentence(proba_passive=0, proba_conj_act=0, proba_conj_tar=0):

	sentence = ""
	policy = Policy()
	
	# Choice of the sentence structure
	is_passive = random.random() < proba_passive
	r = random.random()
	is_conj_act = r < proba_conj_act
	is_conj_tar = r >= proba_conj_act and r < proba_conj_tar
	if is_passive:
		structure = ["targets", "modalities", "actions_p"]
	else:
		structure = ["actors", "modalities", "actions", "targets"]
	
	mod = None
	assignee = None
	targets = []
	actions = []
	for lex_name in structure:
		tok, uri = pick_from_lex(lex_name)
		sentence += tok + " "
		if lex_name == "modalities":
			mod = uri
		elif lex_name == "actors":
			assignee = uri
		elif lex_name == "targets":
			targets.append(uri)
			if is_conj_tar:
				tok, uri = pick_new_from_lex(lex_name, tok)
				sentence += f"and {tok} "
				targets.append(uri)
		else:
			actions.append(uri)
			if is_conj_act:
				tok, uri = pick_new_from_lex(lex_name, tok)
				sentence += f"and {tok} "
				actions.append(uri)
	
	policy.rules.append(Rule(mod, assignee, actions, targets))
	sentence = sentence[:-1] # Remove last " "
	return (sentencify(sentence), str(policy))



if __name__ == "__main__":

	PROBA_PASSIVE = 0.1
	PROBA_CONJ_ACT = 0.2
	PROBA_CONJ_TAR = 0.2

	if len(sys.argv) < 2 :
		print(f"Usage: python3 {sys.argv[0]} <number of sentences> [<.txt output directory>] [<ODRL output directory>] [<file prefix>]")
		exit()
	
	number = int(sys.argv[1])
	raw_out_dir = "../automatic-examples/" if len(sys.argv) <= 2 else sys.argv[2]
	if raw_out_dir[-1] != "/": raw_out_dir += "/"
	odrl_out_dir = "../../data/automatic-examples/" if len(sys.argv) <= 3 else sys.argv[3]
	if odrl_out_dir[-1] != "/": odrl_out_dir += "/"
	prefix = "ex" if len(sys.argv) <= 4 else sys.argv[4]
	
	print(f"- Probability of no assignee: {PROBA_PASSIVE}")
	print(f"- Probability of several actions: {PROBA_CONJ_ACT}")
	print(f"- Probability of several targets: {PROBA_CONJ_TAR}")
	
	for i in range(23, number+1):
		print(f"Generating sentence {i}/{number}...", end="\r")
		filename_raw = raw_out_dir + prefix + str(i) + ".txt"
		filename_odrl = odrl_out_dir + prefix + str(i) + ".ttl"
		sentence, policy = gen_sentence(PROBA_PASSIVE, PROBA_CONJ_ACT, PROBA_CONJ_TAR)
		with open(filename_raw, "w") as fout:
			fout.write(sentence)
		with open(filename_odrl, "w") as fout:
			fout.write(policy)
	
	print("\nDone")
	
