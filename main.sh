#!/bin/bash

usage_message="Usage: $(basename $0) -i <input> -a <amr_dir> -o <output_dir> [-t <odrl>]"
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Option parsing
while getopts ':i:a:o:t:' OPTION; do
  case "$OPTION" in
    i)
      input="$OPTARG"
      echo "Input: $input"
      ;;
    a)
      amr="$OPTARG"
      echo "AMR: $amr"
      ;;
    o)
      output="$OPTARG"
      echo "Output: $output"
      ;;
    t)
      gt_odrl="$OPTARG"
      echo "Ground Truth ODRL: $gt_odrl"
      ;;
    ?)
      echo $usage_message
      exit 1
      ;;
  esac
done

echo

# Checking the parameters are ok
if [[ $input == "" || $amr == "" || $output == "" ]]; then
	echo $usage_message
	exit 1
fi

# Checking if the input exists
if [[ ! -e "$input" ]]; then
	echo "Input $input not found"
	exit 1
fi

# Checking if the ODRL dir exists
if [[ $gt_odrl != "" && ! -e "$gt_odrl" ]]; then
	echo "Ground Truth ODRL $gt_odrl not found"
	exit 1 
fi

# Checking if input and odrl have the same type
if [[ -d $input && -f $gt_odrl ]]; then
	echo "Input $input is a directory, but ground truth ODRL $gt_odrl is a file"
	exit 1
fi
if [[ -f $input && -d $gt_odrl ]]; then
	echo "Input $input is a file, but ground truth ODRL $gt_odrl is a directory"
	exit 1
fi


# Transformations

echo "Parsing into AMR..."
python3 "$script_dir/text_to_amr.py" "$input" "$amr"


echo "Transforming into ODRL using GREW..."
sh "$script_dir/grew_system/main_grew.sh" $amr $output

if [[ $gt_odrl != "" ]]; then
	echo "Computing stats and scores..."
	echo
	if [[ -d $input ]]; then
		python3 "$script_dir/scorer/main.py" "$input" "$gt_odrl" "$output"
	else
		# put everything in directories
		mkdir "$input-dir"
		cp "$input" "$input-dir"
		mkdir "$gt_odrl-dir"
		cp "$gt_odrl" "$gt_odrl-dir"
		
		python3 "$script_dir/scorer/main.py" "$input-dir" "$gt_odrl-dir" "$output"
		
		rm -r "$gt_odrl-dir" "$input-dir"
	fi
fi
