import penman
import graphviz
import sys

if len(sys.argv) < 3:
	print(f"Usage: {sys.argv[0]} <input_file (penman)> <output_file (pdf)>")
	exit(1)


with open(sys.argv[1], "r") as finput:
	pen_text = finput.read()
	pen_graph = penman.decode(pen_text)
	
	graph = graphviz.Digraph("AMR", filename=sys.argv[1] + ".gv", format = "pdf")
	
	for con1, role, con2 in pen_graph.triples:
		if role == ":instance":
			for i in range(len(pen_graph.triples)):
				cept1 = pen_graph.triples[i][0]
				cept2 = pen_graph.triples[i][2]
				if cept1 == con1:
					cept1 = con2
				if cept2 == con1:
					cept2 = con2
				pen_graph.triples[i] = (cept1, pen_graph.triples[i][1], cept2)
	
	for con1, role, con2 in pen_graph.triples:
		if role != ":instance":
			graph.edge(con1, con2, label=role)
	
	graph.render(outfile = "./" + sys.argv[2], view = True)
