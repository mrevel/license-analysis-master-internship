# Automated License Analysis

This repository contains the work of Malo Revel's master internship for the [CLARA project](https://project.inria.fr/clara/). It consists of a system that automatically parses natural language sentences taken from licenses into an [ODRL](https://www.w3.org/TR/odrl-model/) representation. It uses [AMR](amr.isi.edu/) as an intermediary semantic representation, and it uses [GREW](https://grew.fr/) for the ODRL transformation.

## Requirements

In order to use the system the following programs need to be installed :
* Python at least 3.11.4
* GREW at least 1.11.0 and libgrew 1.11.0 (see the [installation instructions](https://grew.fr/usage/install/))
* [AMRLib](https://github.com/bjascob/amrlib/) as well as an [AMRLib model](https://github.com/bjascob/amrlib-models/). The model used in the internship is `model_parse_xfm_bart_large-v0_1_0`
* The following Python modules: `rdflib`, `graphviz` and `penman` (optional, for the visualisation), `amrlib`, `json`

## Usage

The script `main.sh` automatically processes sentences and goes through all the steps of the workflow to produce ODRL representations of the sentences. It can either process a single sentence in a file or an entire folder of sentences. The command to use it is the following:
```
> sh main.sh -i <input> -a <amr> -o <output> [-t <ground_truth_odrl>]
```
Where:
* `input` is either a path to a sentence file or a path to a directory containing sentence files (the files preferably end with the extension `.txt`)
* `amr` is a directory (that is created if non-existing) which will contain the intermediary AMR representations of the sentences
* `output` is a directory (that is created if non-existing) which will contain the target ODRL representations of the sentences
* `odrl` (optional) is a directory (or a single file if the input is a single file) containing the ground truth ODRL representations of the input sentences. Using this option makes the system compare the output ODRL representations with the ground truth and print the scores in the standard input.

## Tenet

A second system for this task has also been developped during the internship. This second system is based on the Tenet framework provided by [Tetras Libre](https://tetras-libre.fr/). The two projects that perform this task are the following:
* [AMRBatch](https://gitlab.tetras-libre.fr/tetras-mars/amrbatch) which transforms NL into AMR using AMRLib and AMR-LD (not a contribution of the internship)
* [Tenet](https://gitlab.tetras-libre.fr/tetras-mars/tenet/-/tree/experiment/clara) (on the CLARA branch)

## Folders description

Here are globally described the different folders and files of the repository.

* `amr_visu.py` is a small script to visualise AMR files and convert them into PDF. To use it:
```
> python3 amr_visu.py <input.amr> <output.pdf>
```
* `datasets/` contains the different datasets created during the internship:
	* `dataset_simple` is the main dataset of 100 simple sentences, with their natural language and ODRL representation.
	* `dataset_complex` is a dataset of 10 sophisticated sentences taken from real license texts. These sentences are annotated in AMR and ODRL.
	* `corpus_gen` is the program to automatically generate simple sentences
* `grew_system/` is the transformation system itself. It contains the GREW transformation rules as well as the GREW final requests and the ODRL generation script.
It also contains a bash script to realise all these operations.
* `main.sh` is the main script of the system, that realises the whole process (cf. Usage)
* `README.md` is the document that you are reading
* `results/` contains the ODRL results for the Tenet and the GREW system on the simple dataset.
* `scorer/` is the script that computes statistics and compares the output ODRL with the ground truth
* `text_to_amr.py` realises the natural language to AMR transformation, using AMRLib
