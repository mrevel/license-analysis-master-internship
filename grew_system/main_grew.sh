#!/bin/bash

# $1: input directory containing the AMR
# $2: output directory (created if nonexisting)
# $3 (optional): "debug" if you want debug info

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ "${1: -1}" == "/" ]
then
	input_dir="$1"
else
	input_dir="$1/"
fi

if [ "${2: -1}" == "/" ]
then
	output_dir="$2"
else
	output_dir="$2/"
fi

mkdir -p $output_dir
for amr in $input_dir*; do
	if [ "${amr: -4}" == ".amr" ]
	then
		echo "Processing AMR $amr:"
		amr_fname=$(basename "$amr")
		amr_bn="${amr_fname:0:-4}"
		graph_fp_bn="$output_dir$amr_bn"
		grew transform -i $amr -o $graph_fp_bn.json -grs $script_dir/grew_transform/main.grs -json -track_rules
		echo "	Graph transformation to $graph_fp_bn.json done."
		grew grep -i $graph_fp_bn.json -request $script_dir/grew_requests/permission.req > "$graph_fp_bn"_perm.json
		grew grep -i $graph_fp_bn.json -request $script_dir/grew_requests/obligation.req > "$graph_fp_bn"_obli.json
		grew grep -i $graph_fp_bn.json -request $script_dir/grew_requests/prohibition.req > "$graph_fp_bn"_proh.json
		python3 $script_dir/json_to_odrl.py "$graph_fp_bn" > $graph_fp_bn.ttl
		echo "	ODRL graph extraction to $graph_fp_bn.ttl done."
		if [ "$3" == "debug" ]
		then
			python3 visu_amr_json.py $graph_fp_bn.json $graph_fp_bn.pdf
			echo "Final graph:"
			cat $graph_fp_bn.json
		else
			rm -f $output_dir*.json $output_dir*.pdf $output_dir*.gv
			echo "	Temporary files removed."
		fi
	fi
done
