#!/bin/bash

grew grep -i $1.json -request final_requests/permission.req > $1_perm.json
grew grep -i $1.json -request final_requests/obligation.req > $1_obli.json
grew grep -i $1.json -request final_requests/prohibition.req > $1_proh.json
