import rdflib, sys, json, os
from rdflib.namespace import RDF, ODRL2

namespaces = {
	"odrl": "http://www.w3.org/ns/odrl/2/",
	"cc": "http://creativecommons.org/ns#"
}
filepath = f"{os.path.dirname(os.path.abspath(__file__))}"
act_lex_fname = f"{filepath}/actions.json"

def get_grep_json(base_fname):
	grep_perm_fname = f"{base_fname}_perm.json"
	grep_obli_fname = f"{base_fname}_obli.json"
	grep_proh_fname = f"{base_fname}_proh.json"
	with open(grep_perm_fname, "r") as fgrep:
		perm_data = json.load(fgrep)
	with open(grep_obli_fname, "r") as fgrep:
		obli_data = json.load(fgrep)
	with open(grep_proh_fname, "r") as fgrep:
		proh_data = json.load(fgrep)
	return perm_data, obli_data, proh_data
	

def get_actions_from_grep(grep_json, modal_kw, jsong):
	actions = []
	for match in grep_json:
		nodes = match["matching"]["nodes"]
		if modal_kw in nodes:
			ident = nodes["ACT"]
			action = jsong["nodes"][ident]["action"]
			actions.append(action)
	return actions


def odrlg_add_actions(odrlg, modality, actions):
	bn = rdflib.BNode()
	if modality == "perm":
		odrlg.add((root, ODRL2.permission, bn))
	elif modality == "obli":
		odrlg.add((root, ODRL2.obligation, bn))
	elif modality == "proh":
		odrlg.add((root, ODRL2.prohibition, bn))
	
	for act in actions:
		if act in actions_lex:
			domain = actions_lex[act]
			node_act = rdflib.URIRef(f"{namespaces[domain]}{act}")
		else:
			node_act = rdflib.Literal(act)
		odrlg.add((bn, ODRL2.action, node_act))
	


if __name__ == "__main__":

	base_fname = sys.argv[1]
	pseudo_amr_fname = f"{base_fname}.json"
	with open(pseudo_amr_fname, "r") as famr:
		jsong = json.load(famr)
	if type(jsong) == list:
		print(f"Error: expected one graph but {len(jsong)} graphs were provided")
		exit(1)


	# Actions lexicon
	with open(act_lex_fname, "r") as fact:
		actions_lex = json.load(fact)

	# GREW grep patterns initialisation
	perm_grep, obli_grep, proh_grep = get_grep_json(base_fname)
	permissions = get_actions_from_grep(perm_grep, "PERM", jsong)
	obligations = get_actions_from_grep(obli_grep, "OBLI", jsong)
	prohibitions = get_actions_from_grep(proh_grep, "PROH", jsong)


	# ODRL graph initialisation
	odrlg = rdflib.Graph()
	odrlg.bind("rdf", RDF)
	odrlg.bind("odrl", ODRL2)
	odrlg.bind("cc", rdflib.Namespace(namespaces["cc"]))
	root = rdflib.Literal("License")
	odrlg.add((root, RDF.type, ODRL2.Policy))


	if obligations != []:
		odrlg_add_actions(odrlg, "obli", obligations)
	if prohibitions != []:
		odrlg_add_actions(odrlg, "proh", prohibitions)
	if permissions != []:
		odrlg_add_actions(odrlg, "perm", permissions)


	print(odrlg.serialize())
