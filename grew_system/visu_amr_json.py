# A script to visualize a JSON AMR-like graph as a PDF

import json
import graphviz
import sys

if len(sys.argv) < 3:
	print(f"Usage: {sys.argv[0]} <input_file (json)> <output_file (pdf)>")
	exit(1)


def amr_node_text(dic):
	if "value" in dic: return dic["value"]
	elif "concept" in dic: return dic["concept"]
	else:
		print(f"Error: node {dic} is incorrect")
		exit(1)

with open(sys.argv[1], "r") as finput:
	jsong = json.load(finput)
	
graph = graphviz.Digraph("AMR", filename=sys.argv[1] + ".gv", format = "pdf")

for edge in jsong["edges"]:
	src_node = amr_node_text(jsong["nodes"][edge["src"]])
	src_text = edge['src'] + "_" + src_node
	tar_node = amr_node_text(jsong["nodes"][edge["tar"]])
	tar_text = edge['tar'] + "_" + tar_node
	lab = edge["label"]
	graph.edge(src_text, tar_text, label=lab)

graph.render(outfile = "./" + sys.argv[2], view = True)
