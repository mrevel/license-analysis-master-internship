# This script parses natural language sentences into AMR graphs using AMRLib.
# It can take as input either:
#    - a single file that is a sentence
#    - a directory that contains files that are sentences
# The output is a directory. It is created if it does not exist

import amrlib, sys, os

def del_empty_lines(text):
	lines = text.split("\n")
	f_lines = [line for line in lines if line != ""]
	return "\n".join(f_lines)

if __name__ == "__main__":

	if len(sys.argv) < 3:
		print(f"Usage: python3 {sys.argv[0]} <input> <output>")
		print("<input> and <output> can be either files or directories")
		exit(1)
	
	in_path = sys.argv[1]
	out_path = sys.argv[2]
	if out_path[-1] != "/": out_path += "/"
	
	if os.path.isfile(in_path):
		is_file = True
	elif os.path.isdir(in_path):
		is_file = False
		if in_path[-1] != "/": in_path += "/"
	else:
		print(f"File or directory {in_path} not found")
		exit(1)
	
	print("Loading AMRLib model...")
	stog = amrlib.load_stog_model()
	
	if not os.path.exists(out_path):
		os.makedirs(out_path)
	
	
	if is_file:
		basename = in_path.split("/")[-1].split(".")[0]
		with open(in_path, "r") as in_file:
			sent = in_file.read()
			
		print(f'Parsing sentence "{sent}" with AMRLib...')
		graph = stog.parse_sents([sent])[0]
		
		with open(f"{out_path}{basename}.amr", "w") as out_file:
			out_file.write(del_empty_lines(str(graph)))
	
	else:
		
		fnames = os.listdir(in_path)
		for fname in fnames:
			basename = fname.split(".")[0]
			
			with open(f"{in_path}{fname}", "r") as in_file:
				sent = in_file.read()
				
			print(f'Parsing sentence "{sent}" with AMRLib...')
			graph = stog.parse_sents([sent])[0]
			
			with open(f"{out_path}{basename}.amr", "w") as out_file:
				out_file.write(del_empty_lines(str(graph)))
				
	print("...Done")
