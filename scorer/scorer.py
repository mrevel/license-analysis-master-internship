# Compares some results (e.g. output of Tenet) w.r.t a corpus
# with the expected ODRL representations of the corpus
# 
# ODRL files in the corpus directory should have the same name as in the
# output directory

import os
import sys
from rdflib import Graph

def f_measure(precision, recall):
	if precision in [0, None] or recall in [0, None]: return None
	else: return 2/(1/precision + 1/recall)

class ODRL:
	
	def __init__(self):
		self.has_perm = False
		self.has_obl = False
		self.has_proh = False
		self.perm = []
		self.obl = []
		self.proh = []
	
	""" Parses the content of an ODRL file and adds its contents to the ODRL """
	def parse(self, odrl_fname):
		graph = Graph()
		graph.parse(odrl_fname)
		odrl_ns = "http://www.w3.org/ns/odrl/2/"
		ccrel_ns = "http://creativecommons.org/ns#"
		perm_nodes = []
		obl_nodes = []
		proh_nodes = []
		actions = {} # action listing for every blank node
		
		# List actions and modalities
		for (src, prop, tgt) in graph:
			src, prop, tgt = str(src), str(prop), str(tgt)
			if prop == odrl_ns + "permission":
				self.has_perm = True
				perm_nodes.append(tgt)
			elif prop == odrl_ns + "obligation":
				self.has_obl = True
				obl_nodes.append(tgt)
			elif prop == odrl_ns + "prohibition":
				self.has_proh = True
				proh_nodes.append(tgt)
			elif prop == odrl_ns + "action":
				if src not in actions: actions[src] = []
				actions[src].append(tgt)
		
		# Link modalities with actions
		for node in actions:
			if node in perm_nodes: mod = self.perm
			elif node in obl_nodes: mod = self.obl
			elif node in proh_nodes: mod = self.proh
			else: print("Warning: ill-formed ODRL")
			for act in actions[node]:
				mod.append(act)
	
	def actions(self):
		acts = []
		for act in self.perm: acts.append(act)
		for act in self.obl: acts.append(act)
		for act in self.proh: acts.append(act)
		return acts
	
	def __str__(self):
		s = ""
		if self.has_perm:
			s += "Permissions:\n"
			for act in self.perm:
				s += f"\t{act}\n"
		if self.has_obl:
			s += "Obligations:\n"
			for act in self.obl:
				s += f"\t{act}\n"
		if self.has_proh:
			s += "Prohibitions:\n"
			for act in self.proh:
				s += f"\t{act}\n"
		return s


class Scores:

	def __init__(self):
		self.scores = {}
		
	def check_and_add_criterion(self, crit):
		if crit not in self.scores:
			self.scores[crit] = {"tp": 0, "fp": 0, "fn": 0}
	
	""" Adds a single measure on the score.
	    tgt (bool): real value
	    output (bool): measured value """
	def add_measure(self, crit, tgt, output):
		self.check_and_add_criterion(crit)
		if tgt and output:
			self.scores[crit]["tp"] += 1
		elif not tgt and output:
			self.scores[crit]["fp"] += 1
		elif tgt and not output:
			self.scores[crit]["fn"] += 1
	
	def get_precision(self, crit):
		tp = self.scores[crit]["tp"]
		fp = self.scores[crit]["fp"]
		if tp + fp != 0:
			return tp/(tp + fp)
		else: return None
	
	def get_recall(self, crit):
		tp = self.scores[crit]["tp"]
		fn = self.scores[crit]["fn"]
		if tp + fn != 0:
			return tp/(tp + fn)
		else: return None
	
	def get_total_precision(self):
		tot_tp = 0
		tot_fp = 0
		for crit in self.scores:
			tot_tp += self.scores[crit]["tp"]
			tot_fp += self.scores[crit]["fp"]
		if tot_tp + tot_fp != 0:
			return tot_tp/(tot_tp + tot_fp)
		else: return None
			
	def get_total_recall(self):
		tot_tp = 0
		tot_fn = 0
		for crit in self.scores:
			tot_tp += self.scores[crit]["tp"]
			tot_fn += self.scores[crit]["fn"]
		if tot_tp + tot_fn != 0:
			return tot_tp/(tot_tp + tot_fn)
		else: return None
	
	def __str__(self):
		s = ""
		for crit in self.scores:
			precision = self.get_precision(crit)
			recall = self.get_recall(crit)
			s += f"{crit}:\n"
			s += f'\tTP = {self.scores[crit]["tp"]}, FP = {self.scores[crit]["fp"]}, FN = {self.scores[crit]["fn"]}\n'
			s += f'\tPrecision = {precision}, Recall = {recall}, F-measure = {f_measure(precision, recall)}\n'
		precision = self.get_total_precision()
		recall = self.get_total_recall()
		s += f'Total precision = {precision}\n'
		s += f'Total recall = {recall}\n'
		s += f'Total F-measure = {f_measure(precision, recall)}\n'
		return s
		


def add_comp_modalities(odrl_target, odrl_output, scores):
	scores.add_measure("permission", odrl_target.has_perm, odrl_output.has_perm)
	scores.add_measure("obligation", odrl_target.has_obl, odrl_output.has_obl)
	scores.add_measure("prohibition", odrl_target.has_proh, odrl_output.has_proh)

def add_comp_actions(odrl_target, odrl_output, scores):
	actions_target = odrl_target.actions()
	actions_output = odrl_output.actions()
	
	for action in actions_output:
		scores.add_measure(action, action in actions_target, True)
	for action in actions_target:
		if action not in actions_output: # Don't reconsider True, True
			scores.add_measure(action, True, False)

def add_comp_global(odrl_target, odrl_output, scores):
	same = set(odrl_target.perm) == set(odrl_output.perm)
	same = same and set(odrl_target.obl) == set(odrl_output.obl)
	same = same and set(odrl_target.proh) == set(odrl_output.proh)
	scores.add_measure("global", True, same)


""" path_target: path to the ODRL files of the corpus
    path_output: path to the ODRL files of the output """
def main_scorer(path_target, path_output):
	
	scores_modalities = Scores()
	scores_actions = Scores()
	scores_global = Scores()
	
	for fname in os.listdir(path_target):
		if fname.endswith(".ttl"):
			print(fname)
			odrl_target = ODRL()
			odrl_target.parse(path_target + fname)
			odrl_output = ODRL()
			odrl_output.parse(path_output + fname)
			
			add_comp_modalities(odrl_target, odrl_output, scores_modalities)
			add_comp_actions(odrl_target, odrl_output, scores_actions)
			add_comp_global(odrl_target, odrl_output, scores_global)
	
	return scores_modalities, scores_actions, scores_global



if __name__ == "__main__":

	if len(sys.argv) < 3:
		print(f"Usage: python3 {sys.argv[0]} <path_corpus_odrl> <path_output_odrl>")
		exit(1)

	path_target = sys.argv[1]
	if path_target[-1] != "/": path_target += "/"
	path_output = sys.argv[2]
	if path_output[-1] != "/": path_output += "/"
	
	scores_modalities, scores_actions, scores_global = main_scorer(path_target, path_output)
	
	print(f"Modality scores:\n{scores_modalities}")
	print(f"Actions scores:\n{scores_actions}")
	print(f"Global scores:\n{scores_global}")
