import stats
import scorer

import os
import sys

if __name__ == "__main__":

	if len(sys.argv) < 4:
		print(f"Usage:\n\t$ python3 {sys.argv[0]} <corpus_sentences_path> <corpus_odrl_path> <output_odrl_path>")
		print("Where:\n\t<corpus_sentences_path> is the path to the .txt source of the corpus")
		print("\t<corpus_odrl_path> is the path to the .ttl data of the corpus")
		print("\t<output_odrl_path> is the path to the .ttl output of the system on the corpus source")
		exit(1)
		
	path_sentences = sys.argv[1]
	if path_sentences[-1] != "/": path_sentences += "/"
	path_target = sys.argv[2]
	if path_target[-1] != "/": path_target += "/"
	path_output = sys.argv[3]
	if path_output[-1] != "/": path_output += "/"
	
	statistics = stats.main_stats(path_sentences, path_target)
	
	scores_modalities, scores_actions, scores_global = scorer.main_scorer(path_target, path_output)
	
	print(f"Corpus source: {path_sentences}\nCorpus data:   {path_target}\nSystem output: {path_output}\n")
	print("############ STATS ############\n")
	print(statistics)
	print("\n############ SCORES ###########\n")
	print(f"# Modality scores:\n{scores_modalities}")
	print(f"# Actions scores:\n{scores_actions}")
	print(f"# Global scores:\n{scores_global}")
